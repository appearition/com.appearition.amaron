﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetPosition : MonoBehaviour
{
    public GameObject arObject;

    public Transform originalPos;
    public Transform originalRotation;
    public Transform originalScale;

    public void ResetPos()
    {
        arObject.transform.position = originalPos.transform.position;
        arObject.transform.rotation = originalPos.transform.rotation;
        arObject.transform.localScale = originalPos.transform.localScale;
    }
}
